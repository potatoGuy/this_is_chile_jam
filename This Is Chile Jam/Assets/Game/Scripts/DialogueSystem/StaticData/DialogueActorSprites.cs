﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace GameJam.Dialogue
{
    [CreateAssetMenu(menuName = ScriptableObjectsConstants.BASE_FOLDER_NAME + "Dialogue/Actor Sprites", fileName = "ActorSprites")]
    public class DialogueActorSprites : ScriptableObject
    {
        public enum ActorExpression
        {
            NORMAL = 0,
            HAPPY = 1,
            ANGRY = 2,
            SAD = 3,
            THINKING = 4
        }

        [Serializable]
        public struct ExpressionData
        {
            [SerializeField]
            private Sprite sprite;
            public Sprite Sprite => sprite;
            [SerializeField]
            private ActorExpression expression;
            public ActorExpression Expression => expression;
        }

        [SerializeField]
        private List<ExpressionData> expressions;


        public Sprite GetSpriteByExpression(ActorExpression expression)
        {
            return expressions
                .Where(_expression => _expression.Expression == expression)
                .FirstOrDefault().Sprite;
        }
    }

}
