using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Dialogue
{
    [CreateAssetMenu(menuName = ScriptableObjectsConstants.BASE_FOLDER_NAME + "Dialogue/Dialogue Sequence", fileName = "DialogueSequence")]
    public class DialogueSequence : ScriptableObject
    {
        [SerializeField]
        private List<Dialogue> dialogues;
        public IReadOnlyCollection<Dialogue> Dialogues => dialogues.AsReadOnly();
    }

}
