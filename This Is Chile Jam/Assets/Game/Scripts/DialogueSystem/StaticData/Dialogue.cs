﻿using UnityEngine;
using static GameJam.Dialogue.DialogueActorSprites;

namespace GameJam.Dialogue
{
    [CreateAssetMenu(menuName = ScriptableObjectsConstants.BASE_FOLDER_NAME + "Dialogue/Dialogue", fileName = "Dialogue")]
    public class Dialogue : ScriptableObject
    {
        [SerializeField]
        [TextArea]
        private string text;
        public string Text => text;

        [SerializeField]
        private ActorExpression leftCharacterExpression;
        public ActorExpression LeftCharacterExpression => leftCharacterExpression;

        [SerializeField]
        private ActorExpression rightCharacterExpression;
        public ActorExpression RightCharacterExpression => rightCharacterExpression;

        [SerializeField]
        private DialogueActorData speakingCharacter;
        public string SpeakingCharacterName => speakingCharacter.Name;

        [SerializeField]
        private DialogueActorData leftCharacter;
        public DialogueActorData LeftCharacter => leftCharacter;
        [SerializeField]
        private DialogueActorData rightCharacter;
        public DialogueActorData RightCharacter => rightCharacter;

    }

}
