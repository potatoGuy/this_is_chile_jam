﻿using UnityEngine;
using static GameJam.Dialogue.DialogueActorSprites;

namespace GameJam.Dialogue
{
    [CreateAssetMenu(menuName = ScriptableObjectsConstants.BASE_FOLDER_NAME + "Dialogue/Actor Data", fileName = "ActorData")]
    public class DialogueActorData : ScriptableObject
    {
        [SerializeField]
        private string actorName;
        public string Name => actorName;
        [SerializeField]
        private string nick;
        public string Nick => nick;
        [SerializeField]
        private DialogueActorSprites actorSprites;

        public Sprite GetExpressionSprite(ActorExpression expression)
        {
            return actorSprites.GetSpriteByExpression(expression);
        }
    }

}
