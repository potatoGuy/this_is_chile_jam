﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace GameJam.Dialogue
{
    public static class WriteUIText
    {
        public const float TIME_BETWEEN_CHARS = 0.04f;
        private static bool stopText = false;
        private static float currentTime = 0.0f;
        public static bool IsRunning { get; private set; }

        public static IEnumerator RunTextRoutine(string message, TextMeshProUGUI textMesh, string submitAxe, AudioSource audioSource, AudioClip[] onTypedSFX)
        {
            stopText = false;
            IsRunning = true;

            #region BUSCAR Y SEPARAR EL MENSAJE BUSCADO TAGS
            List<string> splitMessage = new List<string>();

            int initialIndex = 0;

            int tag_mod = 1;

            for (int i = 0; i < message.Length; i++)
            {
                int finalIndex;
                if (message[i] == '<')
                {
                    if (i == 0)
                    { tag_mod = 0; }
                    finalIndex = i;
                    int temp_count = finalIndex - initialIndex;
                    if (temp_count > 0)
                    {
                        char[] temp_split = new char[temp_count];
                        message.CopyTo(initialIndex, temp_split, 0, temp_count);
                        splitMessage.Add(new string(temp_split));
                        initialIndex = i;
                    }
                }

                if (message[i] == '>')
                {
                    finalIndex = i;
                    int temp_count = finalIndex - initialIndex;
                    if (temp_count > 0)
                    {
                        char[] temp_split = new char[temp_count + 1];
                        message.CopyTo(initialIndex, temp_split, 0, temp_count + 1);
                        splitMessage.Add(new string(temp_split));
                        initialIndex = i + 1;
                    }
                }
                if (i == message.Length - 1)
                {
                    finalIndex = i;
                    int temp_count = finalIndex - initialIndex;
                    if (temp_count > 0)
                    {
                        char[] temp_split = new char[temp_count + 1];
                        message.CopyTo(initialIndex, temp_split, 0, temp_count + 1);
                        splitMessage.Add(new string(temp_split));
                        initialIndex = i + 1;
                    }
                }
            }

            #endregion

            #region MOSTRAR MENSAJE SIN TAGS
            if (splitMessage.Count == 0)
            {
                string temp_message = "";
                for (int i = 0; i < message.Length; i++)
                {
                    currentTime = 0f;
                    if (submitAxe != null && Input.GetButtonDown(submitAxe))
                    { stopText = true; }
                    if (stopText)
                    { break; }

                    temp_message += message[i];
                    textMesh.SetText(temp_message);

                    if (audioSource != null && !audioSource.isPlaying)
                    {
                        AudioClip temp_sfx = onTypedSFX[Random.Range(0, onTypedSFX.Length)];
                        audioSource.clip = temp_sfx;
                        audioSource.Play();
                    }
                    while (currentTime < TIME_BETWEEN_CHARS)
                    {
                        if (submitAxe != null && Input.GetButtonDown(submitAxe))
                        { stopText = true; }
                        currentTime += Time.smoothDeltaTime;
                        if (stopText)
                        { break; }
                        yield return new WaitForFixedUpdate();
                    }
                    Debug.Log((int)message[i]);
                    if (stopText)
                    { break; }
                }

                IsRunning = false;
                textMesh.SetText(message);
                yield return null;
            }
            #endregion

            #region MOSTRAR MENSAJE CON TAGS
            else
            {
                string finalMessage = "";
                int skip_index = -1;
                int insert_index = 0;

                for (int i = 0; i < splitMessage.Count; i++)
                {
                    if (i == skip_index)
                    { continue; }

                    if (i % 2 == tag_mod)
                    {
                        finalMessage += splitMessage[i] + "";
                        insert_index = finalMessage.Length;
                        finalMessage += splitMessage[i + 2];
                        skip_index = i + 2;
                        textMesh.SetText(finalMessage);
                    }
                    else
                    {
                        for (int j = 0; j < splitMessage[i].Length; j++)
                        {
                            currentTime = 0f;
                            if (submitAxe != null && submitAxe != "" && Input.GetButtonDown(submitAxe))
                            { stopText = true; }
                            if (stopText)
                            { break; }

                            string temp_text = "";
                            temp_text += splitMessage[i][j];
                            finalMessage = finalMessage.Insert(insert_index, temp_text);
                            insert_index++;
                            textMesh.SetText(finalMessage);

                            if (audioSource != null && !audioSource.isPlaying)
                            {
                                AudioClip temp_sfx = onTypedSFX[Random.Range(0, onTypedSFX.Length)];
                                audioSource.clip = temp_sfx;
                                audioSource.Play();

                            }

                            while (currentTime < TIME_BETWEEN_CHARS)
                            {
                                if (submitAxe != null && submitAxe != "" && Input.GetButtonDown(submitAxe))
                                { stopText = true; }
                                currentTime += Time.smoothDeltaTime;
                                if (stopText)
                                { break; }
                                yield return new WaitForFixedUpdate();
                            }
                            if (stopText)
                            { break; }
                        }
                        insert_index = finalMessage.Length;
                    }

                }

                IsRunning = false;
                textMesh.SetText(message);
                yield return null;
            }
            #endregion
        }

        public static void StopWhileRunning()
        {
            stopText = true;
        }
    }
}
