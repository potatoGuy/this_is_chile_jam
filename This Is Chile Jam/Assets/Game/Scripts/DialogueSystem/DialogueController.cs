﻿using System.Collections;
using UnityEngine;

namespace GameJam.Dialogue
{
    public class DialogueController : MonoBehaviour
    {
        [SerializeField]
        private DialogueUI dialogueUI;

        public void DisplayDialogue(DialogueSequence dialogueSequence)
        {
            StartCoroutine(DisplayDialogueRoutine(dialogueSequence));
        }
        private IEnumerator DisplayDialogueRoutine(DialogueSequence dialogueSequence)
        {
            foreach (Dialogue dialogue in dialogueSequence.Dialogues)
            {
                yield return StartCoroutine(dialogueUI.DisplayDialogue(dialogue));
            }
        }
    }
}
