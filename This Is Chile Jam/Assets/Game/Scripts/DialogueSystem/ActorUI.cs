﻿using System;
using UnityEngine;
using UnityEngine.UI;
using static GameJam.Dialogue.DialogueActorSprites;

namespace GameJam.Dialogue
{
    [Serializable]
    public struct ActorUI
    {
        [SerializeField]
        private Image actorImage;

        public void SetData(DialogueActorData actorData, ActorExpression expression)
        {
            actorImage.sprite = actorData.GetExpressionSprite(expression);
            actorImage.gameObject.SetActive(true);
        }

        public void Disable()
        {
            actorImage.gameObject.SetActive(false);
        }
    }
}
