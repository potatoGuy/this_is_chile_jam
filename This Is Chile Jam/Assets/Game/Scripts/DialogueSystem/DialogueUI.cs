﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace GameJam.Dialogue
{
    public class DialogueUI : MonoBehaviour
    {
        [SerializeField]
        private ActorUI leftActorUI;
        [SerializeField]
        private ActorUI rightActorUI;
        [SerializeField]
        private TextMeshProUGUI dialogueText;
        [SerializeField]
        private TextMeshProUGUI nameText;

        public IEnumerator DisplayDialogue(Dialogue dialogue)
        {
            nameText.text = dialogue.SpeakingCharacterName;

            if (dialogue.LeftCharacter != null)
                leftActorUI.SetData(dialogue.LeftCharacter, dialogue.LeftCharacterExpression);
            else
                leftActorUI.Disable();

            if (dialogue.RightCharacter != null)
                rightActorUI.SetData(dialogue.RightCharacter, dialogue.RightCharacterExpression);
            else
                rightActorUI.Disable();


            yield return WriteUIText.RunTextRoutine(dialogue.Text, dialogueText, "Horizontal", null, null);
        }
    }
}
